<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//start frontend routes

Route::get('/','frontendController\HomeController@index');

// end frontend routes



// start admin routes

Route::get('/admin','adminController\AdminController@login')->name('login');
Route::post('/admin-login-check','adminController\AdminController@adminLoginCheck')->name('adminLoginCheck');
Route::get('/dashboard','adminController\SuperAdmincontroller@dashboard')->name('dashboard');
Route::get('logout','adminController\SuperAdmincontroller@logout')->name('logout');

// end admin route


// Category routes

Route::get('/add-cagegory','adminController\CategoryController@addCategory')->name('add-category');
Route::post('/save-category','adminController\CategoryController@saveCategory')->name('save-category');
Route::get('manage-category','adminController\CategoryController@manageCategory')->name('manage-category');
Route::get('/unpublished-category/{id}','adminController\CategoryController@unpublishedCategory');
Route::get('/published-category/{id}','adminController\CategoryController@publishedCategory');
Route::get('/edit-category/{id}','adminController\CategoryController@editCategory');
Route::post('/update-category','adminController\CategoryController@updateCategory');
Route::get('/delete-category/{id}','adminController\CategoryController@deleteCategory');

// Post routes
Route::get('/add-post','adminController\PostController@addPost')->name('add-post');
Route::post('/save-post','adminController\PostController@savePost');
Route::get('manage-post','adminController\PostController@managePost')->name('manage-post');
Route::get('/unpublished-post/{id}','adminController\PostController@unpublishedPost');
Route::get('/published-post/{id}','adminController\PostController@publishedPost');
Route::get('/edit-post/{id}','adminController\PostController@editPost');
Route::post('/update-post','adminController\PostController@updatePost');
Route::get('/delete-post/{id}','adminController\PostController@deletePost');
Route::get('/post-details/{id}','frontendController\HomeController@postDetails');
Route::get('/show-same-category-post/{id}','frontendController\HomeController@showSameCategoryPost');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
