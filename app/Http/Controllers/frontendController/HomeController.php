<?php

namespace App\Http\Controllers\frontendController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class HomeController extends Controller
{
    public function index(){


      $all_post_info = DB::table('posts')
                      ->where('publication_status',1)
                      ->orderBy('post_id','desc')
                      ->get();

      return view('frontEnd.home.index')
                 ->with('all_post_info',$all_post_info);

    }

    public function postDetails($post_id){

      $single_post_details = DB::table('posts')
                            ->where('post_id',$post_id)
                            ->first();

      $data['hit_counter'] = $single_post_details->hit_counter+1;

        DB::table('posts')
            ->where('post_id',$post_id)
            ->update($data);

      return view('frontEnd.post.postDetails')
                  ->with('single_post_details',$single_post_details);
                }
    public function showSameCategoryPost($category_id){

      $all_post_info = DB::table('posts')
                      ->where('publication_status',1)
                      ->where('category_id',$category_id)
                      ->orderBy('post_id','desc')
                      ->get();

      return view('frontEnd.home.index')
                 ->with('all_post_info',$all_post_info);

    }
}
