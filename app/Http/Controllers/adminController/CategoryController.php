<?php

namespace App\Http\Controllers\adminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class CategoryController extends Controller
{
    public function addCategory(){
      $this->authCheck();
      return view('admin.category.addCategory');
    }

    private function authCheck(){
      $admin_id = Session::get('admin_id');
      if($admin_id!=null){
        return;
      }

      else {
        return redirect()->route('login')->send();
      }
    }

    public function saveCategory(Request $request){
      $data = array();
      $data['category_name'] = $request->category_name;
      $data['category_description'] = $request->category_description;
      $data['publication_status'] = $request->publication_status;
      DB::table('categories')->insert($data);
      return redirect()->route('add-category')->with('message','Category added successfully');
    }

    public function manageCategory(){
      $this->authCheck();
      $all_category_info = DB::table('categories')->get();
      return view('admin.category.manageCategory')->with('all_category_info',$all_category_info);
    }

    public function unpublishedCategory($category_id){
      DB::table('categories')
          ->where('id',$category_id)
          ->update(['publication_status'=>0]);
      return redirect()->route('manage-category');
    }

    public function publishedCategory($category_id){
      DB::table('categories')
          ->where('id',$category_id)
          ->update(['publication_status'=>1]);
      return redirect()->route('manage-category');
    }

    public function editCategory($category_id){
      $category_info = DB::table('categories')
                           ->where('id',$category_id)
                           ->first();
      return view('admin.category.editCategory')->with('category_info',$category_info);
    }

    public function updateCategory(Request $request){
      $data = array();
      $category_id= $request->category_id;
      $data['category_name'] = $request->category_name;
      $data['category_description'] = $request->category_description;
      DB::table('categories')
          ->where('id',$category_id)
          ->update($data);
      return redirect()->route('manage-category');
    }

    public function deleteCategory($category_id){
      DB::table('categories')
          ->where('id',$category_id)
          ->delete();
      return redirect()->route('manage-category');
    }
}
