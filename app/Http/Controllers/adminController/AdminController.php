<?php

namespace App\Http\Controllers\adminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
session_start();

class AdminController extends Controller
{
    public function login(){
       $this->authCheck();
        return view('admin.login');
      }

    private function authCheck(){
      $admin_id = Session::get('admin_id');
      if($admin_id){
        return redirect()->route('dashboard')->send();
      }

      else {
        return;
      }
    }

    public function adminLoginCheck(Request $request){

      $admin_email = $request->admin_email;
      $admin_password = $request->admin_password;

      $result = DB::table('admins')
                     ->where('admin_email',$admin_email)
                     ->where('admin_password',md5($admin_password))
                     ->first();


       //dd($result);

       if($result){
         Session::put('admin_id',$result->admn_id);
         Session::put('admin_name',$result->admin_name);

         return redirect()->route('dashboard');
       }

      else {
        Session::put('exception','Email or Password invalid');

        return redirect()->route('login');
      }


    }
}
