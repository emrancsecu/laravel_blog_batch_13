<?php

namespace App\Http\Controllers\adminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
class PostController extends Controller
{
    public function addPost(){
      $all_category = DB::table('categories')->get();
      return view('admin.post.addPost')->with('all_category',$all_category);
    }

    public function savePost(Request $request){

      $data = array();
      $data['category_id'] = $request->category_id;
      $data['author_name'] = Session::get('admin_name');
      $data['post_title'] = $request->post_title;
      $data['post_short_description'] = $request->post_short_description;
      $data['post_long_description'] = $request->post_long_description;
      $data['publication_status'] = $request->publication_status;

      /* Image upload */

      $file = $request->file('post_image');
      $filename = $file->getClientOriginalName();
      $extension = $file->getClientOriginalExtension();
      $image = date('His').$filename;
      $imageUrl = 'public/post_image/'.$image;
      $destinationPath = base_path().'/public/post_image/';
      $imageUploadSuccess = $file->move($destinationPath,$image);

      if($imageUploadSuccess){
        $data['post_image'] = $imageUrl;
        DB::table('posts')->insert($data);
        return redirect()->route("add-post")->with('message','post saved successfully');
      }

      else{
        $error = $file->getErrorMessage();
      }

    }

    public function managePost(){

      $all_post_info = DB::table('posts')->get();

      return view('admin.post.managePost')->with('all_post_info',$all_post_info);
    }

    public function unpublishedPost($post_id){
      DB::table('posts')
           ->where('post_id',$post_id)
           ->update(['publication_status'=>0]);
      return redirect()->route('manage-post');
    }

    public function publishedPost($post_id){
      DB::table('posts')
          ->where('post_id',$post_id)
          ->update(['publication_status'=>1]);
      return redirect()->route('manage-post');
    }

    public function editPost($post_id){
      $post_info = DB::table('posts')
                           ->where('post_id',$post_id)
                           ->first();
      $category_info = DB::table('categories')->get();
      return view('admin.post.editPost')
                   ->with('post_info',$post_info)
                   ->with('category_info',$category_info);
    }

    public function updatePost(Request $request){
      $data = array();
      $post_id = $request->post_id;
      $data['category_id']= $request->category_id;
      $data['post_title'] = $request->post_title;
      $data['post_short_description'] = $request->post_short_description;
      $data['post_long_description'] = $request->post_long_description;

      /*
      echo "<pre>";
      print_r($_FILES);
      echo "</pre>";
      exit();
      */

      /* Image upload */
      if($_FILES['post_image']['name']==''){
         $data['post_image'] = $request->post_old_image;
        DB::table('posts')
            ->where('post_id',$post_id)
            ->update($data);
        return redirect()->route('manage-post')->with('message','post updated successfully');

      }
      else{
        $file = $request->file('post_image');
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $image = date('His').$filename;
        $imageUrl = 'public/post_image/'.$image;
        $destinationPath = base_path().'/public/post_image/';
        $imageUploadSuccess = $file->move($destinationPath,$image);
        if($imageUploadSuccess){
        $data['post_image'] = $imageUrl;
          DB::table('posts')
              ->where('post_id',$post_id)
              ->update($data);
        unlink($request->post_old_image);
          return redirect()->route('manage-post')->with('message','post updated successfully');
        }

        else{
          $error = $file->getErrorMessage();
        }
      }
    }

    public function deletePost($post_id){
      DB::table('posts')
          ->where('post_id',$post_id)
          ->delete();
      return redirect()->route('manage-post');
    }
}
