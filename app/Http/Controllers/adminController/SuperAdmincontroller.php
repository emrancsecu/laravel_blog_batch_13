<?php

namespace App\Http\Controllers\adminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
session_start();
class SuperAdmincontroller extends Controller
{
    public function dashboard(){
      $this->authCheck();
       return view('admin.home.admin_home');
    }

    private function authCheck(){
      $admin_id = Session::get('admin_id');
      if($admin_id){
        return;
      }

      else {
        return redirect()->route('login')->send();
      }
    }

    public function logout(){
      Session::put('admin_id','');
      Session::put('admin_name','');
      Session::put('message','You are successfully logout');

      return redirect()->route('login');

    }
}
