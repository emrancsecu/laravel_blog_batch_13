@extends('frontEnd.layouts.master')

@section('main_content')

              <div class="templatemo_post_wrapper">
              <div class="templatemo_post">
                  <div class="post_title">
                    {{$single_post_details->post_title}}
                  </div>
                  <div class="post_info">
                    Posted by <a href="#" target="_blank">{{$single_post_details->author_name}}</a>, {{$single_post_details->created_at}}
                  </div>
                  <div class="post_body">
                      <img src="{{asset($single_post_details->post_image)}}" alt="free css template" border="1" width="500" height="200"/>
                      <p>{{$single_post_details->post_long_description}}</p>
                </div>
              <div class="post_comment">
                    <a href="#">No Comment</a>
                  </div>
              </div>
              </div> <!-- End of a post-->

@endsection
