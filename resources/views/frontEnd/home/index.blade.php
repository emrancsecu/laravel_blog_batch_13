@extends('frontEnd.layouts.master')

@section('main_content')
              @foreach($all_post_info as $postInfo)
              <div class="templatemo_post_wrapper">
              <div class="templatemo_post">
                  <div class="post_title">
                    <a href="{{URL::to('/post-details/'.$postInfo->post_id)}}">{{$postInfo->post_title}}</a>
                  </div>
                  <div class="post_info">
                    Posted by <a href="#" target="_blank">{{$postInfo->author_name}}</a>, {{$postInfo->created_at}}
                  </div>
                  <div class="post_body">
                      <img src="{{asset($postInfo->post_image)}}" alt="free css template" border="1" width="500" height="200"/>
                      <p>{{$postInfo->post_short_description}}</p>
                </div>
              <div class="post_comment">
                    <a href="#">No Comment</a>
                  </div>
              </div>
              </div> <!-- End of a post-->
             @endforeach

@endsection
