<div id="templatemo_header_panel">
    <div id="templatemo_title_section">
      <h1>OLD BLOG</h1>
  Your tagline goes here</div>
  </div> <!-- end of templatemo header panel -->

  <div id="templatemo_menu_panel">
    <div id="templatemo_menu_section">
          <ul>
              <li><a href="{{URL::to('/')}}"  class="current">Home</a></li>
              <li><a href="#">Gallery</a></li>
              <li><a href="#">Categories</a></li>
              <li><a href="#">Archives</a></li>
              <li><a href="#">About</a></li>
              <li><a href="#">Contact</a></li>
              @guest
              <li><a href="{{URL::to('/login')}}">Login</a></li>
              <li><a href="{{URL::to('/register')}}">Register</a></li>

              @else
              <li>
                    <a href="{{URL::to('/logout')}}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>

              </li>
              @endguest
          </ul>
  </div>
  </div> <!-- end of menu -->
