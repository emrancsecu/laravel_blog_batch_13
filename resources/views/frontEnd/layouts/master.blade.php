<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Old Blog Template - Free CSS Layout</title>
<meta name="keywords" content="free css layout, old blog template, CSS, HTML" />
<meta name="description" content="Old Blog Template - free website template provided by TemplateMo.com" />
<link href="{{asset('public/frontEnd/css/templatemo_style.css')}}" rel="stylesheet" type="text/css" />
<!--  Designed by w w w . t e m p l a t e m o . c o m  -->
<link rel="stylesheet" type="text/css" href="{{asset('public/frontEnd/css/tabcontent.css')}}" />
<!--for automatic auth-->
<link rel="stylesheet" type="text/css" href="{{asset('public/css/app.css')}}"/>
<script type="text/javascript" src="{{asset('public/frontEnd/js/tabcontent.js')}}">
/***********************************************
* Tab Content script v2.2- © Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/
</script>
</head>
<body>

	@include('frontEnd.layouts.header')

	<div id="templatemo_content_container">
        <div id="templatemo_content">
            <div id="templatemo_content_left">

                 @yield('main_content')

                <!-- End of a post-->

            </div> <!-- end of content left -->

            @include('frontEnd.layouts.sidebar')

             <!-- end of right content -->
	    </div> <!-- end of content -->
    </div> <!-- end of content container -->

	@include('frontEnd.layouts.footer')
