<div id="templatemo_content_right">

  <div class="templatemo_right_section">
<div class="tag_section">
            <ul id="countrytabs" class="shadetabs">
                <li><a href="#" rel="search" class="selected">Search</a></li>
                <li><a href="#" rel="category">Category</a></li>
                <li><a href="#" rel="archive">Archive</a></li>
            </ul>
</div>

        <div class="tabcontent_section">
            <div id="search" class="tabcontent">
                <form method="get" action="#">
                    <input class="inputfield" name="searchkeyword" type="text" id="searchkeyword"/>
                    <input type="submit" name="submit" class="button" value="Search" />
                </form>
            </div>

            <div id="category" class="tabcontent">
              @php
                $all_category_info = DB::table('categories')
                                    ->where('publication_status',1)
                                    ->get();
              @endphp
                <ul>
                   @foreach($all_category_info as $category_info)
                    <li><a href="{{URL::to('/show-same-category-post/'.$category_info->id)}}">{{$category_info->category_name}}</a></li>
                   @endforeach
                </ul>
            </div>

            <div id="archive" class="tabcontent">
                <ul>
                    <li><a href="#">January 2049</a></li>
                    <li><a href="#">December 2048</a></li>
                    <li><a href="#">November 2048</a></li>
                    <li><a href="#">October 2048</a></li>
                    <li><a href="#">September 2048</a></li>
                </ul>
            </div>
</div>

<script type="text/javascript">

        var countries=new ddtabcontent("countrytabs")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()

        </script> <!--- end of tag -->
    </div>


    <div class="templatemo_right_section">
      <h2>Popular Posts</h2>
        @php

         $popular_posts = DB::table('posts')
                        ->where('publication_status',1)
                        ->orderBy('hit_counter','desc')
                        ->limit(5)
                        ->get();

        @endphp
      <ul>
        @foreach($popular_posts as $popular_post)
        <li><a href="{{URL::to('/post-details/'.$popular_post->post_id)}}">{{$popular_post->post_title}}</a>({{$popular_post->hit_counter}})</li>
        @endforeach
      </ul>
    </div>

    <div class="templatemo_right_section">
      @php
        $all_recnt_posts = DB::table('posts')
                     ->orderBy('post_id','desc')
                     ->limit(5)
                     ->get();
      @endphp
      <h2>Recent Posts</h2>
        <ul>
           @foreach($all_recnt_posts as $recnt_post)
            <li><a href="{{URL::to('/post-details/'.$recnt_post->post_id)}}">{{$recnt_post->post_title}}</a></li>
          @endforeach
        </ul>
    </div>

    <div class="templatemo_right_section">
      <h2>Recent Comments</h2>
      <ul>
            <li>Lorem Ipsum on <a href="#">Donec mollis aliquet</a></li>
            <li>Consectetuer on <a href="#">Suspendisse a nibh</a></li>
            <li>Sed on <a href="#">Pellentesque vitae magna</a></li>
            <li>Vitae Neque on <a href="#">Nunc blandit orci sit amet</a></li>
          <li>Donec Mollis on <a href="#">Maecenas adipiscing</a></li>
      </ul>
    </div>

    <div class="templatemo_right_section">
      <h2>Search</h2>
<form method="get" action="#">
            <input class="inputfield" name="keyword" type="text" id="keyword"/>
            <input type="submit" name="submit" class="button" value="Search" />
        </form>
    </div>

</div>
