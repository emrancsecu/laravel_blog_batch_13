@extends('admin.layouts.admin_master')

@section('main_content')

<div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
               <div class="span12">

                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class="page-title text-center text-info">
                       Add Category Form
                   </h3>

                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->


            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="widget green">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> Add Category Form</h4>

                        </div>
                        <h3 class="text-center text-success">{{ Session::get('message') }}</h3>

                        <div class="widget-body form">
                            <!-- BEGIN FORM-->
                              {!! Form::open(['url' => '/save-category','method'=>'POST','class'=>'cmxform form-horizontal']) !!}
                                <div class="control-group ">
                                    <label for="category_name" class="control-label">Category Name:</label>
                                    <div class="controls">
                                        <input class="span6 " id="category_name" name="category_name" type="text" placeholder="Add Category Name" required/>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="category_description" class="control-label">Category Description:</label>
                                    <div class="controls">
                                        <textarea class="span6" id="category_description" rows="5" name="category_description" placeholder="Add Category Description..." required></textarea>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="publication_status" class="control-label">Publication Status:</label>
                                    <div class="controls">
                                        <select class="span6" id="publication_status" name="publication_status">
                                              <option value="1">Publish</option>
                                              <option value="0">Unpublish</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button class="btn btn-success" type="submit">Save</button>
                                    <button class="btn" type="button">Cancel</button>
                                </div>
                              {!! Form::close() !!}
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->

         </div>
         <!-- END PAGE CONTAINER-->
      </div>

@endsection
