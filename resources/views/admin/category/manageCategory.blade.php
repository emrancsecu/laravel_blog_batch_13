@extends('admin.layouts.admin_master')

@section('main_content')

<div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
               <h3 class="text-center">Manage Category</h3>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->

            <div id="page-wraper">
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget orange">
                            <div class="widget-title">
                                <h4 class="text-center"> Manage Category</h4>
                            </div>
                            <div class="widget-body">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>

                                    <tr>
                                        <th>Cagegory Id</th>
                                        <th> Category Name </th>
                                        <th> Category Description </th>
                                        <th> Publication Status</th>
                                        <th>Action</th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    @foreach($all_category_info as $categoryInfo)
                                    <tr>
                                        <td>{{$categoryInfo->id}}</td>
                                        <td>{{$categoryInfo->category_name}}</td>
                                        <td>{{$categoryInfo->category_description}}</td>
                                        <td>
                                          @if($categoryInfo->publication_status==1)
                                         <span class="label label-success label-mini">publish</span>
                                         @else
                                         <span class="label label-important label-mini">unpublish</span>
                                         @endif
                                        </td>
                                        <td>
                                            @if($categoryInfo->publication_status==1)
                                            <a href="{{URL::to('/unpublished-category/'.$categoryInfo->id)}}" title="unpublish"><button class="btn btn-danger"><i class="icon-thumbs-down"></i></button></a>
                                            @else
                                            <a href="{{URL::to('/published-category/'.$categoryInfo->id)}}" title="publish"><button class="btn btn-success"><i class="icon-thumbs-up"></i></button></a>
                                            @endif
                                            <a href="{{URL::to('/edit-category/'.$categoryInfo->id)}}" title="edit"><button class="btn btn-primary"><i class="icon-pencil"></i></button></a>
                                            <a href="{{URL::to('/delete-category/'.$categoryInfo->id)}}" onclick="return checkDelete()" title="delete"><button class="btn btn-danger"><i class="icon-trash "></i></button></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
                </div>

            </div>

            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>

@endsection
