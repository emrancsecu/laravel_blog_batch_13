@extends('admin.layouts.admin_master')

@section('main_content')

<div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
               <div class="span12">

                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class="page-title text-center text-info">
                       Edit Post Form
                   </h3>

                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->


            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="widget green">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> Edit Post Form</h4>

                        </div>
                        <h3 class="text-center text-success">{{ Session::get('message') }}</h3>

                        <div class="widget-body form">
                            <!-- BEGIN FORM-->
                              {!! Form::open(['url' => '/update-post','method'=>'POST','class'=>'cmxform form-horizontal','name'=>'edit_post','enctype'=>'multipart/form-data']) !!}
                                <div class="control-group ">
                                    <label for="category_name" class="control-label">Post Title:</label>
                                    <div class="controls">
                                        <input class="span6 " id="post_title" name="post_title" type="text" placeholder="Add Post Title" value="{{$post_info->post_title}}" required/>
                                        <input type="hidden" name="post_id" value="{{$post_info->post_id}}"/>
                                        <input type="hidden" name="post_old_image" value="{{$post_info->post_image}}"/>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="post_image" class="control-label">Post Image:</label>
                                    <div class="controls">
                                        <input class="span6 " id="post_image" name="post_image" type="file"/>
                                        <span><img src="{{asset($post_info->post_image)}}" width="100"/></span>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="category_name" class="control-label">Category Name:</label>
                                    <div class="controls">
                                        <select class="span6" id="category_id" name="category_id">
                                              @foreach($category_info as $categoryInfo)
                                              <option value="{{$categoryInfo->id}}">{{$categoryInfo->category_name}}</option>
                                              @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="post_short_description" class="control-label">Short Description:</label>
                                    <div class="controls">
                                        <textarea class="span6" id="post_short_description" rows="4" name="post_short_description" placeholder="Add Post Short Description..." required>
                                           {{$post_info->post_short_description}}
                                        </textarea>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="post_long_description" class="control-label">Long Description:</label>
                                    <div class="controls">
                                        <textarea class="span6" id="post_long_description" rows="8" name="post_long_description" placeholder="Add Post long Description..." required>
                                            {{$post_info->post_long_description}}
                                        </textarea>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <button class="btn btn-success" type="submit">update</button>
                                    <button class="btn" type="button">Cancel</button>
                                </div>
                              {!! Form::close() !!}
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->

         </div>
         <!-- END PAGE CONTAINER-->
      </div>

      <script type="text/javascript">
        document.forms['edit_post'].elements['category_id'].value="{{$post_info->category_id}}";
      </script>

@endsection
