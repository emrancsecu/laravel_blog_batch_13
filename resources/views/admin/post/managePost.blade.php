@extends('admin.layouts.admin_master')

@section('main_content')

<div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
               <h3 class="text-center">Manage Post</h3>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->

            <div id="page-wraper">
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget orange">
                            <div class="widget-title">
                                <h4 class="text-center"> Manage Post</h4>
                            </div>
                            <h3 class="text-info text-center">{{Session::get('message')}}</h3>
                            <div class="widget-body">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>

                                    <tr>
                                        <th>Post Id</th>
                                        <th>Cagegory Id</th>
                                        <th> Post Title </th>
                                        <th> Post Short Description </th>
                                        <th>Post Long Description</th>
                                        <th>Post Image</th>
                                        <th> Publication Status</th>
                                        <th>Action</th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    @foreach($all_post_info as $postInfo)
                                    <tr>
                                        <td>{{$postInfo->post_id}}</td>
                                        <td>{{$postInfo->category_id}}
                                        <td>{{$postInfo->post_title}}</td>
                                        <td>{{$postInfo->post_short_description}}</td>
                                        <td>{{$postInfo->post_long_description}}
                                        <td><img src="{{asset($postInfo->post_image)}}" width="50" height="50"/></td>
                                        <td>
                                          @if($postInfo->publication_status==1)
                                         <span class="label label-success label-mini">publish</span>
                                         @else
                                         <span class="label label-important label-mini">unpublish</span>
                                         @endif
                                        </td>
                                        <td>
                                            @if($postInfo->publication_status==1)
                                            <a href="{{URL::to('/unpublished-post/'.$postInfo->post_id)}}" title="unpublish"><button class="btn btn-danger"><i class="icon-thumbs-down"></i></button></a>
                                            @else
                                            <a href="{{URL::to('/published-post/'.$postInfo->post_id)}}" title="publish"><button class="btn btn-success"><i class="icon-thumbs-up"></i></button></a>
                                            @endif
                                            <a href="{{URL::to('/edit-post/'.$postInfo->post_id)}}" title="edit"><button class="btn btn-primary"><i class="icon-pencil"></i></button></a>
                                            <a href="{{URL::to('/delete-post/'.$postInfo->post_id)}}" onclick="return checkDelete()" title="delete"><button class="btn btn-danger"><i class="icon-trash "></i></button></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
                </div>

            </div>

            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>

@endsection
