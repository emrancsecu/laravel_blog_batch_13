@extends('admin.layouts.admin_master')

@section('main_content')

<div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
               <div class="span12">

                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class="page-title text-center text-info">
                       Add Post Form
                   </h3>

                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->


            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="widget green">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> Add Post Form</h4>

                        </div>
                        <h3 class="text-center text-success">{{ Session::get('message') }}</h3>

                        <div class="widget-body form">
                            <!-- BEGIN FORM-->
                              {!! Form::open(['url' => '/save-post','method'=>'POST','enctype'=>'multipart/form-data','class'=>'cmxform form-horizontal']) !!}
                                <div class="control-group ">
                                    <label for="post_title" class="control-label">Post Title:</label>
                                    <div class="controls">
                                        <input class="span6 " id="post_title" name="post_title" type="text" placeholder="Add Post Title" required/>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="post_image" class="control-label">Post Image:</label>
                                    <div class="controls">
                                        <input class="span6 " id="post_image" name="post_image" type="file"/>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="category_name" class="control-label">Category Name:</label>
                                    <div class="controls">
                                        <select class="span6" id="category_id" name="category_id">
                                              @foreach($all_category as $categoryInfo)
                                              <option value="{{$categoryInfo->id}}">{{$categoryInfo->category_name}}</option>
                                              @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="post_short_description" class="control-label">Short Description:</label>
                                    <div class="controls">
                                        <textarea class="span6" id="post_short_description" rows="4" name="post_short_description" placeholder="Add Post Short Description..." required></textarea>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="post_long_description" class="control-label">Long Description:</label>
                                    <div class="controls">
                                        <textarea class="span6" id="post_long_description" rows="8" name="post_long_description" placeholder="Add Post long Description..." required></textarea>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="publication_status" class="control-label">Publication Status:</label>
                                    <div class="controls">
                                        <select class="span6" id="publication_status" name="publication_status">
                                              <option value="1">Publish</option>
                                              <option value="0">Unpublish</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button class="btn btn-success" type="submit">Save</button>
                                    <button class="btn" type="button">Cancel</button>
                                </div>
                              {!! Form::close() !!}
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->

         </div>
         <!-- END PAGE CONTAINER-->
      </div>

@endsection
